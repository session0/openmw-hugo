---
title: "Downloads"
date: 2022-05-26
author: "openmw"
slug: "downloads"
description: ""
summary: ""
draft: false

toc: false

opengraph:
    title: ""
    description: "" # 155 Characters
    image: ""   # Recommended Size: 1200px x 630px
    alt: ""

twitter:
    title: ""
    description: "" # 155 Characters
    image: ""  # Recommended Size: 1024px x 512px

---
## Release Versions

These versions are official releases that have been thoroughly tested.

### Windows / MacOS / Linux

-   [Download from GitHub](https://github.com/OpenMW/openmw/releases/latest) (available at the end of the changelog)
-   [Download from site file repository](https://downloads.openmw.org/?C=M&O=D)

### Other Download Locations

-   **Windows:** [Install via Chocolatey](https://chocolatey.org/packages/openmw)
-   **Ubuntu:** [Official Ubuntu Repo](https://packages.ubuntu.com/search?keywords=openmw) **/** [Launchpad PPA](https://launchpad.net/~openmw/+archive/openmw)
-   **Debian:** [Official Debian Repo](https://packages.debian.org/sid/openmw)
-   **Arch Linux:** [Install from \[community\] repository](https://www.archlinux.org/packages/?sort=&q=openmw)
-   **Gentoo:** [Install from Gentoo repository](https://packages.gentoo.org/package/games-engines/openmw)
-   **OpenSUSE:** [Install from OpenSUSE repository](https://software.opensuse.org/download.html?project=games&package=openmw)
-   **Fedora:** [Install from RPM Fusion repository](https://admin.rpmfusion.org/pkgdb/package/free/openmw/)

Also available as a [Flatpak](http://flatpak.org/) package from [Flathub](https://flathub.org/). You can either [click to download](https://flathub.org/repo/appstream/org.openmw.OpenMW.flatpakref), or use the command line:  
```text
flatpak install --from https://flathub.org/repo/appstream/org.openmw.OpenMW.flatpakref
```

## Development Builds

These are development builds of OpenMW with changes currently not present in the latest release.

**Note**: These builds contain the newest features but are also significantly less-tested than the release builds.

If you find an issue while using an Unreleased Build, please visit our issue tracker to see if it’s been reported, and report it if it has not been.

-   [Windows builds](https://gitlab.com/OpenMW/openmw/-/jobs/artifacts/master/raw/OpenMW_MSVC2019_64_RelWithDebInfo_master.zip?job=Windows_MSBuild_RelWithDebInfo)
-   [Make sure Visual C++ redist is installed](https://aka.ms/vs/16/release/vc_redist.x64.exe)

Packaged Linux Distro Unreleased Builds:

-   [Ubuntu Linux Launchpad PPA](https://launchpad.net/~openmw/+archive/ubuntu/openmw-daily)
-   [Arch Linux AUR](https://aur.archlinux.org/packages/?O=0&K=openmw)

## OpenMW User Manual

Latest version of the [OpenMW user manual](https://openmw.readthedocs.io/en/master/manuals/index.html).

## OpenMW-CS User Manual

Latest version of the [OpenMW-CS user manual](https://openmw.readthedocs.io/en/master/manuals/openmw-cs/index.html).
