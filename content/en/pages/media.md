---
title: "Pretty Screenshots"
date: 2022-09-04
author: "openmw"
slug: "media"
description: "Open source Elderscrolls 3: Morrowind engine reimplementation."
summary: ""
draft: false
toc: false
internalScripts:
    - js/gallery.js

opengraph:
    title: ""
    description: "" # 155 Characters
    image: ""   # Recommended Size: 1200px x 630px
    alt: ""

twitter:
    title: ""
    description: "" # 155 Characters
    image: ""  # Recommended Size: 1024px x 512px

---

{{< gallery >}}

## Release commentaries and videos
Our official YouTube channel, named [MrOpenMW](https://www.youtube.com/user/MrOpenMW), is where the release commentaries are uploaded. We try to make two videos (one for OpenMW and one for OpenMW-CS) for every release, covering the most important changes to the engine. The commentaries are currently made by the brilliant Atahualpa, and have been for the last few years. Most videos up to version 0.35 were made by the legendary WeirdSexy. Filling in for Atahualpa for the 0.46.0 release, the terrific johnnyhostile has now joined the team to collaborate with Atahualpa in future OpenMW videos.

{{< youtube id="nk1YmN70jfc" title="Lysol's Vivec Normal Mapped 1.0" >}}
{{< youtube id="NjfCxa2tpjY" title="Morrowind: Vtastek Water shaders for OpenMW 2021 1440" >}}
{{< youtube id="Yj6jK6TuCCI" title="Morrowind Remastered 2021: OpenMW 0.47.0 Graphics Overhaul, Distant Land, Shadows, Parallax, Shaders" >}}
