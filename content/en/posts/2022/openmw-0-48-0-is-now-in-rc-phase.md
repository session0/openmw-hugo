---
title: "OpenMW 0.48.0 is now in RC-phase!"
date: 2022-09-07
author: "jvoisin"
slug: "openmw-roadmap-updatopenmw-0-48-0-is-now-in-rc-phase"
description: ""
summary: ""
draft: false

opengraph:
    title: ""
    description: "" # 155 Characters
    image: ""   # Recommended Size: 1200px x 630px
    alt: ""

twitter:
    title: ""
    description: "" # 155 Characters
    image: ""  # Recommended Size: 1024px x 512px

---
Finally, after much hard work to get all the cool new features ready for release, we have our first round of Release Candidates, or “RC” for short. RCs are simply release binaries for testing, to make sure everything is in order for a release. That’s right, testing! So we would be _**very**_ grateful if you would download an RC for your OS of choice, test it a bit to see if it works without any issues and report any findings you make to our GitLab [issue tracker](https://gitlab.com/groups/OpenMW/-/issues). Please make sure to check first that whatever you find is not on the tracker already.

-   [macOS x86\_64 RC2](https://mega.nz/file/yFsxUTiS#X0EF1TqoZITJ9YjgptPuOjwI_pG81TWRlAIJHzUc4TI) (Intel)
-   [macOS arm64 RC2](https://mega.nz/file/uQsBhL5L#CzydtXxtV5yAFMSyQKOUvB1znfi-hoKghJKmtbPAsJU) (M1)
-   [Windows RC2](https://openmw.rgw.ctrl-c.liu.se/Releases/OpenMW-0.48.0-RC2-win64.exe)
-   [Linux x64 generic RC2](https://redfortune.de/openmw/rc/openmw-0.48.0-Linux-64BitRC2-osgdoubleprecision.tar.gz)

Thank you and we’ll see you again on the day of the release!  
To join in on the forum discussion, head over to our [OpenMW 0.48 Topic](https://forum.openmw.org/viewtopic.php?p=72947)
