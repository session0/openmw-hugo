---
title: "OpenMW Special Video: 10,000 YouTube Subscribers"
date: 2021-03-30
author: "Atahualpa"
slug: "openmw-special-video-10000-youtube-subscribers"
description: ""
summary: ""
draft: false

opengraph:
    title: ""
    description: "" # 155 Characters
    image: ""   # Recommended Size: 1200px x 630px
    alt: ""

twitter:
    title: ""
    description: "" # 155 Characters
    image: ""  # Recommended Size: 1024px x 512px

---
Starting with version 0.13.0, each and every main release of our engine has been accompanied by a release commentary. For me and many others, the release videos have always been something to look forward to, a strangely satisfying means to highlight OpenMW’s progress throughout the ages — and a great opportunity to mess around with our beloved Fargoth!

Our channel has now reached 10,000 subscribers, a truly gigantic number for such a niche project like OpenMW. A huge thank-you to all of you for your continuous support! Rest assured, though, that we are not reaching for the stars here and that we will still concentrate on publishing release commentaries in the future instead of featuring puppies or kittens. But a little celebration seems to be appropriate, which is why I present you with an updated version of our 2013 video “A Visual History of Changes to OpenMW”! Sit back, relax, and have fun. — And thanks again for your support!

{{< youtube id="UeJc3e_qQWY" title="A Visual History of OpenMW 2021 | 10k Subscribers Special" >}}

[Want to leave a comment?](https://forum.openmw.org/viewtopic.php?f=38&t=7423)
