// Lightbox/Gallery Functionality
const lightbox = document.getElementById('lightbox');
const closeButton = document.getElementById('lightbox-close');
const caption = document.getElementById('lightbox-caption');

document.querySelectorAll("figure.gallery > a").forEach(item => {
    item.addEventListener("click", event => {
        event.preventDefault();
        
        // Create new image and add data.
        let image = new Image();
        image.src = item.href;
        caption.innerText = item.dataset.caption;

        // Display our lightbox.
        lightbox.style.display = "flex";
        image.setAttribute('id', 'lightbox-image');
        lightbox.appendChild(image);
    })
})

lightbox.addEventListener("click", function () {
    
    // Hide the lightbox and remove image/caption.
    this.style.display = "none";
    caption.innerText = "";
    lightbox.removeChild(lightbox.lastElementChild);

})
